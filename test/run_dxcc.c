// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_add_dxcc_check_count(void **state);
void test_add_dxcc_check_parsed(void **state);
void test_add_dxcc_check_starred(void **state);
void test_add_prefix_check_count(void **state);
void test_add_prefix_check_parsed(void **state);
void test_dxcc_by_index_out_of_bounds(void **state);
void test_dxcc_empty_after_init(void **state);
void test_prefix_by_index_out_of_bounds(void **state);
void test_prefix_empty_after_init(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_dxcc_empty_after_init, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_prefix_empty_after_init, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_prefix_by_index_out_of_bounds, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_dxcc_by_index_out_of_bounds, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_dxcc_check_count, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_dxcc_check_parsed, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_dxcc_check_starred, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_prefix_check_count, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_prefix_check_parsed, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
