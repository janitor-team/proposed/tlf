// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_letters_only_no(void **state);
void test_letters_only_yes(void **state);
void test_nodigit(void **state);
void test_other_callarea(void **state);
void test_other_country(void **state);
void test_reported(void **state);
void test_simple_calls(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_letters_only_yes, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_letters_only_no, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_simple_calls, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_other_callarea, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_other_country, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_nodigit, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_reported, NULL, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
