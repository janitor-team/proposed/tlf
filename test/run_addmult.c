// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_arrlss(void **state);
void test_arrlss_2(void **state);
void test_dx_arrlsections(void **state);
void test_init_mults(void **state);
void test_load_multi(void **state);
void test_load_multi_dos(void **state);
void test_load_multi_file_not_found(void **state);
void test_load_multi_leading_space(void **state);
void test_load_multi_no_file(void **state);
void test_load_multi_redefined(void **state);
void test_load_multi_sorted(void **state);
void test_load_multi_with_alias(void **state);
void test_load_multi_with_emptyalias(void **state);
void test_match_length_match_alias(void **state);
void test_match_length_match_alias2(void **state);
void test_match_length_match_mult(void **state);
void test_match_length_no_match(void **state);
void test_remember_mult_empty(void **state);
void test_remember_mult_one(void **state);
void test_remember_mult_same_2x(void **state);
void test_remember_mult_same_2x_newband(void **state);
void test_remember_mult_two(void **state);
void test_serial_grid4(void **state);
void test_serial_grid4_empty(void **state);
void test_serial_section_mult(void **state);
void test_write_mult(void **state);
void test_wysiwyg_multi(void **state);
void test_wysiwyg_multi_2(void **state);
void test_wysiwyg_multi_empty(void **state);
void test_wysiwyg_once(void **state);
void test_wysiwyg_once_2(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_init_mults, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_remember_mult_empty, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_remember_mult_one, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_remember_mult_two, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_remember_mult_same_2x, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_remember_mult_same_2x_newband, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_write_mult, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_no_file, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_file_not_found, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_dos, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_leading_space, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_sorted, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_redefined, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_with_emptyalias, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_load_multi_with_alias, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_match_length_no_match, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_match_length_match_mult, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_match_length_match_alias, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_match_length_match_alias2, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wysiwyg_once, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wysiwyg_multi, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wysiwyg_multi_empty, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_serial_grid4, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_serial_grid4_empty, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_arrlss, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_serial_section_mult, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_dx_arrlsections, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_arrlss_2, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wysiwyg_once_2, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wysiwyg_multi_2, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
