// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup(void **state);
int setup_default(void **state);
int setup_ssbcw(void **state);
int teardown_default(void **state);
void test_arrl_fd(void **state);
void test_arrldx_usa(void **state);
void test_country_found(void **state);
void test_cqww(void **state);
void test_dupe(void **state);
void test_empty_continentlist(void **state);
void test_in_continentlist(void **state);
void test_in_countrylist(void **state);
void test_in_countrylist_keeps_countrynr(void **state);
void test_not_in_continnetlist(void ** state);
void test_not_in_countrylist(void **state);
void test_scoreByCorC_InList(void **state);
void test_scoreByCorC_continentlistOnly(void **state);
void test_scoreByCorC_countrylistOnly(void **state);
void test_scoreByCorC_notInList(void **state);
void test_simple_points(void **state);
void test_ssbcw(void **state);
void test_wpx(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_dupe, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_wpx, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_cqww, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_arrl_fd, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_simple_points, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_arrldx_usa, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ssbcw, setup_ssbcw, teardown_default),
      cmocka_unit_test_setup_teardown(test_in_countrylist, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_not_in_countrylist, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_in_countrylist_keeps_countrynr, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_country_found, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_empty_continentlist, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_not_in_continnetlist, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_in_continentlist, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_scoreByCorC_countrylistOnly, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_scoreByCorC_continentlistOnly, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_scoreByCorC_notInList, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_scoreByCorC_InList, setup_default, teardown_default),
    };

    return cmocka_run_group_tests(tests, setup, NULL);
}
