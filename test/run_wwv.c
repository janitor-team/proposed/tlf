// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_wwv_check(void **state);
void test_wwv_check_not_wwv(void **state);
void test_wwv_check_wcy(void **state);
void test_wwv_check_wcy_no_gmt(void **state);
void test_wwv_check_wcy_sa(void **state);
void test_wwv_check_wcy_sa_aur(void **state);
void test_wwv_check_wcy_sa_gmf(void **state);
void test_wwv_check_wcy_sa_gmf_aur(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_wwv_check_not_wwv, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy_sa, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy_sa_gmf, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy_sa_gmf_aur, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy_sa_aur, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_wwv_check_wcy_no_gmt, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
