// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_bad_empty(void **state);
void test_bad_nan(void **state);
void test_ok_08(void **state);
void test_ok_14(void **state);
void test_ok__8(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_ok_08, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_ok__8, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_ok_14, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_bad_empty, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_bad_nan, NULL, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
