// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_getband_ok(void **state);
void test_getband_wrongband(void **state);
void test_getmode_error (void **state);
void test_getmode_ignores_case (void **state);
void test_getmode_ok (void **state);
void test_getpoints (void **state);
void test_iscomment(void **state);
void test_isnocomment(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_iscomment, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_isnocomment, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getband_ok, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getband_wrongband, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getmode_ok, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getmode_ignores_case, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getmode_error, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_getpoints, NULL, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
