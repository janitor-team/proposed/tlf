// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_IsWarcIndex(void **state);
void test_bandindex2nr(void **state);
void test_bandnr2index(void **state);
void test_conv_f2b(void ** state);
void test_conv_f2b_borders(void ** state);
void test_conv_f2b_out_of_band(void ** state);
void test_nextBandDown(void **state);
void test_nextBandUp(void **state);
void test_nextBandWrapDownwards(void **state);
void test_nextBandWrapUpwards(void **state);
void test_toBandMask(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_toBandMask, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_IsWarcIndex, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_bandnr2index, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_bandindex2nr, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_nextBandUp, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_nextBandDown, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_nextBandWrapUpwards, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_nextBandWrapDownwards, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_conv_f2b_out_of_band, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_conv_f2b_borders, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_conv_f2b, NULL, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
