// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
int teardown_default(void **state);
void test_check_length_ok(void **state);
void test_check_no_newline(void **state);
void test_check_to_Long(void **state);
void test_file_length_long(void **state);
void test_file_length_ok(void **state);
void test_file_length_short(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_check_length_ok, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_check_to_Long, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_check_no_newline, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_file_length_ok, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_file_length_short, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_file_length_long, setup_default, teardown_default),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
