// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
int teardown_default(void **state);
void test_OnLowerSearchPanel_AllBand(void **state);
void test_OnLowerSearchPanel_contest(void **state);
void test_UsePartialFromCallmaster(void **state);
void test_UsePartialFromLog (void **state);
void test_UsePartialFromLogNotUnique (void **state);
void test_UsePartialNotUnique(void **state);
void test_UsePartialNotUnique_only_callmaster(void **state);
void test_ZoneFromCountry(void **state);
void test_ZoneFromExchange(void **state);
void test_ZoneFromLog(void **state);
void test_ZoneFromLog_mixedmode(void **state);
void test_bandstr2line(void **state);
void test_callmaster_ok(void **state);
void test_callmaster_ok_arrlss(void **state);
void test_callmaster_ok_dos(void **state);
void test_callmaster_ok_spaces(void **state);
void test_displayPartials(void **state);
void test_displayPartials_exact_callmaster(void **state);
void test_init_search_panel_contest(void **state);
void test_init_search_panel_dxped(void **state);
void test_init_search_panel_no_contest(void **state);
void test_searchlog_extract_data(void **state);
void test_searchlog_extract_data_mixedmode(void **state);
void test_searchlog_pickup_call(void **state);
void test_searchlog_pickup_call_mixedmode(void **state);
void test_use_different_callmaster(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_callmaster_ok, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_callmaster_ok_dos, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_callmaster_ok_spaces, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_callmaster_ok_arrlss, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_use_different_callmaster, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_init_search_panel_no_contest, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_init_search_panel_contest, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_init_search_panel_dxped, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_searchlog_pickup_call, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_searchlog_pickup_call_mixedmode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_searchlog_extract_data, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_searchlog_extract_data_mixedmode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_bandstr2line, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_UsePartialFromLog, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_UsePartialFromLogNotUnique, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_UsePartialFromCallmaster, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_UsePartialNotUnique, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_UsePartialNotUnique_only_callmaster, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_displayPartials_exact_callmaster, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_displayPartials, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ZoneFromCountry, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ZoneFromExchange, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ZoneFromLog, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ZoneFromLog_mixedmode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_OnLowerSearchPanel_contest, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_OnLowerSearchPanel_AllBand, setup_default, teardown_default),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
