// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_empty_call(void **state);
void test_from_ielist(void **state);
void test_from_worked(void **state);
void test_not_found(void **state);
void test_respect_nonempty_comment(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_empty_call, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_respect_nonempty_comment, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_not_found, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_from_worked, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_from_ielist, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
