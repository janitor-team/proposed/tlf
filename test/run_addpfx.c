// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_addKnown(void **state);
void test_addKnownMultiband(void **state);
void test_addOne(void **state);
void test_addOneTwoBands(void **state);
void test_addOneTwoTimes(void **state);
void test_addTwoSameBand(void **state);
void test_addTwoTwoBands(void **state);
void test_addUnknown(void **state);
void test_initPfx(void **state);
void test_outOfBand(void ** state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_initPfx, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_outOfBand, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addOne, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addOneTwoTimes, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addOneTwoBands, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addTwoSameBand, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addTwoTwoBands, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addKnown, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addUnknown, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addKnownMultiband, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
