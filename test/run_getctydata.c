// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_abnormal_call(void **state);
void test_is_wpx(void **state);
void test_location_known(void **state);
void test_location_unknown(void **state);
void test_location_unknown_used(void **state);
void test_no_wpx(void **state);
void test_same_result(void **data);
void test_someidea(void **data);
void test_suffix_empty(void **state);
void test_suffix_getctydata(void **state);
void test_suffix_getctynr(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_location_known, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_location_unknown, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_suffix_empty, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_location_unknown_used, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_suffix_getctynr, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_suffix_getctydata, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_someidea, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_same_result, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_no_wpx, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_is_wpx, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_abnormal_call, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
