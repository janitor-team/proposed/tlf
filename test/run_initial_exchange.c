// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
int teardown_default(void **state);
void test_empty_call(void **state);
void test_long_line(void **state);
void test_no_comma(void **state);
void test_no_file(void **state);
void test_ok(void **state);
void test_ok_dos(void **state);
void test_ok_multi_column(void **state);
void test_ok_tab(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_no_file, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ok, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ok_dos, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_long_line, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_no_comma, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ok_tab, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_empty_call, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_ok_multi_column, setup_default, teardown_default),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
