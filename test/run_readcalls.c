// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default (void **state);
void test_lookup_in_pfxnummult(void **state);
void test_lookup_not_in_pfxnummult(void **state);
void test_veto_eclude_none (void **state);
void test_veto_exclude_continent (void **state);
void test_veto_exclude_continent_contlist_only (void **state);
void test_veto_exclude_country (void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_lookup_not_in_pfxnummult, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_lookup_in_pfxnummult, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_veto_eclude_none, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_veto_exclude_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_veto_exclude_continent_contlist_only, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_veto_exclude_continent, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
