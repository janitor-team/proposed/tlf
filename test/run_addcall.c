// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_addcall2_pfxnum_inList(void **state);
int setup_addcall2_pfxnum_notinList(void **state);
int setup_addcall_pfxnum_inList(void **state);
int setup_addcall_pfxnum_notinList(void **state);
int setup_default(void **state);
void test_add2_2_countries(void **state);
void test_add2_2_zones(void **state);
void test_add2_country(void **state);
void test_add2_country_2_band(void **state);
void test_add2_country_2_stations(void **state);
void test_add2_unknown_country(void **state);
void test_add2_unknown_zone(void **state);
void test_add2_warc(void **state);
void test_add2_zone(void **state);
void test_add2_zone_2_band(void **state);
void test_add2_zone_2_stations(void **state);
void test_add_2_countries(void **state);
void test_add_2_zones(void **state);
void test_add_country(void **state);
void test_add_country_2_band(void **state);
void test_add_country_2_stations(void **state);
void test_add_unknown_country(void **state);
void test_add_unknown_zone(void **state);
void test_add_warc(void **state);
void test_add_zone(void **state);
void test_add_zone_2_band(void **state);
void test_add_zone_2_stations(void **state);
void test_addcall2_continentlistonly(void **state);
void test_addcall2_exclude_continent(void **state);
void test_addcall2_exclude_country (void **state);
void test_addcall2_nopfxnum(void **state);
void test_addcall2_pfxnum_inList(void **state);
void test_addcall2_pfxnum_notinList(void **state);
void test_addcall_continentlistonly(void **state);
void test_addcall_exclude_continent(void **state);
void test_addcall_exclude_country (void **state);
void test_addcall_nopfxnum(void **state);
void test_addcall_pfxnum_inList(void **state);
void test_addcall_pfxnum_notinList(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_addcall_nopfxnum, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall_pfxnum_inList, setup_addcall_pfxnum_inList, NULL),
      cmocka_unit_test_setup_teardown(test_addcall_pfxnum_notinList, setup_addcall_pfxnum_notinList, NULL),
      cmocka_unit_test_setup_teardown(test_addcall_continentlistonly, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall_exclude_continent, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall_exclude_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_nopfxnum, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_pfxnum_inList, setup_addcall2_pfxnum_inList, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_pfxnum_notinList, setup_addcall2_pfxnum_notinList, NULL),
      cmocka_unit_test_setup_teardown(test_add_unknown_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_country_2_band, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_country_2_stations, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_2_countries, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_unknown_zone, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_zone, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_zone_2_band, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_zone_2_stations, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_2_zones, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_unknown_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_country, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_country_2_band, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_country_2_stations, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_2_countries, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_unknown_zone, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_zone, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_zone_2_band, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_zone_2_stations, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_2_zones, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add_warc, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_add2_warc, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_continentlistonly, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_exclude_continent, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_addcall2_exclude_country, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
