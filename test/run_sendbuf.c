// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
int teardown_default(void **state);
void test_expandCall(void **state);
void test_expandHisNr(void **state);
void test_expandHisRST(void **state);
void test_expandHisRSTshort(void **state);
void test_expandHiscall(void **state);
void test_expandQsoNr(void **state);
void test_expandQsoNrshort(void **state);
void test_noexpand(void **state);
void test_prepareSPcallCWDeMode(void **state);
void test_prepareSPcallCWnoDeMode(void **state);
void test_prepareSPcallDIGIDeMode(void **state);
void test_prepareSPcallDIGInoDeMode(void **state);
void test_prepareSPcallMFJDeMode(void **state);
void test_prepareSPcallMFJnoDeMode(void **state);
void test_replace_all(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_replace_all, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_noexpand, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandCall, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandHiscall, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandHisRST, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandHisRSTshort, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandQsoNr, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandQsoNrshort, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_expandHisNr, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallCWnoDeMode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallCWDeMode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallDIGInoDeMode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallDIGIDeMode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallMFJnoDeMode, setup_default, teardown_default),
      cmocka_unit_test_setup_teardown(test_prepareSPcallMFJDeMode, setup_default, teardown_default),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
