// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup_default(void **state);
void test_cluster_show_all(void **state);
void test_cluster_show_all_6(void **state);
void test_cluster_show_ann(void **state);
void test_cluster_show_dx(void **state);
void test_cluster_show_talk(void **state);
void test_freqwindow(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_cluster_show_dx, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_cluster_show_talk, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_cluster_show_ann, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_cluster_show_all, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_cluster_show_all_6, setup_default, NULL),
      cmocka_unit_test_setup_teardown(test_freqwindow, setup_default, NULL),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
