// generated on Tue Apr 28 09:04:44 2020
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

int setup(void **state);
void test_cabToTlf_ParseQSO(void **state);
void test_cabToTlf_ParseQTC(void **state);
void test_cabToTlf_ParseXQSO(void **state);
void test_freeCabfmt(void **state);
void test_parseLine(void **state);
void test_readCabrilloFileNotFound(void **state);
void test_readCabrilloFormatNotFound(void **state);
void test_readCabrilloFormatUniversal(void **state);
void test_readCabrilloFormatWAE(void **state);
void test_starts_with_fails(void **state);
void test_starts_with_succeed(void **state);
void test_translateItem(void **state);

int main(void) {
    const struct CMUnitTest tests[] = {
      cmocka_unit_test_setup_teardown(test_starts_with_succeed, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_starts_with_fails, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_translateItem, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_freeCabfmt, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_parseLine, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_readCabrilloFormatUniversal, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_readCabrilloFormatWAE, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_readCabrilloFileNotFound, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_readCabrilloFormatNotFound, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_cabToTlf_ParseQSO, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_cabToTlf_ParseXQSO, NULL, NULL),
      cmocka_unit_test_setup_teardown(test_cabToTlf_ParseQTC, NULL, NULL),
    };

    return cmocka_run_group_tests(tests, setup, NULL);
}
